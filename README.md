# ALTAIR Senior Software developer test 

Carefully read acceptance criteria and for any clarification you might need,
feel free to mail [mgalvez@altair.com](mgalvez@altair.com) for more info.

As a rule of thumb, tests should cover the most part of functionality.


__WordCounter__

This class must count the number of occurrences of each word in the file 'Text.txt' and show them ordered
form more to less frequent with an occurrence count aside.

Acceptance criteria are listed below:

	-	You are totally free to create the project structure and implementation. 
	-	The already implemented classes can be used, edited or replaced in case you need
	-	Suggested output format is specified class WordCounter just to have uniform outcome, but, again, feel free to modify it.
	-	For testing, please use 'getFile(String fileName)' to access any custom-created test resource.

================================================================================

##  Enrique provided solution

I tried to make the code to be self explained well enough in order to promote readability and clean code, instead of providing code comments. Not sure if I achieved it.

### Framework considerations
    
As the project did not come integrated with any framework, I considered, for lightweight, to avoid not needed libraries or dependencies.
Having into account that I do not have clear the purpose of this 'service' I decided to keep it as a library in its simplest way instead of making it a microservice or to depend on a framework such as Spring.

### Project structure

I decided to propose a hexagonal architecture approach for the folder organisation, but as I said, this is just a proposal approach, I do not have enough knowledge about any domain behind the scenes.

I left **Application** layer in blank, as I didn't identify any end user interaction with the system. We could implement there a handler for a user call or external programs (even for function or lambda) that eventually will call the WordCounter service within the domain layer.

Regarding **Infrastructure** layer, I placed there a reader file mechanism, as the specifications for WordCounter explicitly says that it should count words just from there.
I got rid of the path reference to 'src/main/resource' and now it is loaded from ClassLoader.
I also added a ConsoleSlf4j implementation for Console interface on WordCounter domain layer.

Within the **domain** layer, I added the business logic to WordCounter.

### Tests

I made up an Acceptance Test based on the specifications (requirements, acceptance criteria) provided, having into account that some client within the project will call this public entry point on the WordCounter service. Then I developed the solution making use of TDD iterations, refactoring eventually.
These are the acceptance criteria based on what I got from your requirements:

**Given this file lines**:
Lorem ipsum dolor,
Lorem ipsum.
Lorem,

**The system should print this output**:
-  Word: lorem  >> Occurrences: 3
-  Word: ipsum  >> Occurrences: 2
-  Word: dolor  >> Occurrences: 1

Based on this, I covered almost 100% of the business code, to report the code coverage I wanted to include Jacoco. But I did not do it eventually.

### The Gradle dependencies included:

- Junit5
- Mockito
- Slf4j  
- I kept Java 11