package com.altair.exercise.infrastructure;

import com.altair.exercise.infrastructure.files.FileStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {

    private static final Logger log = LoggerFactory.getLogger(PropertiesLoader.class);

    private final FileStorage fileStorage;

    public PropertiesLoader() {
        fileStorage = new FileStorage();
    }

    public Properties getApplicationProperties() {
        try {
            final InputStream input = fileStorage.getResourceAsStream("application.properties");
            final Properties properties = new Properties();
            properties.load(input);
            return properties;
        } catch (NullPointerException e) {
            log.error("Error getting application.properties file", e);
            return null;
        } catch (IOException e) {
            log.error("Error loading properties, e");
            return null;
        }
    }
}
