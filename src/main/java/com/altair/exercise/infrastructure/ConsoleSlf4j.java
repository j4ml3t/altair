package com.altair.exercise.infrastructure;

import com.altair.exercise.domain.wordcounter.Console;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsoleSlf4j implements Console {

    private static final Logger log = LoggerFactory.getLogger(ConsoleSlf4j.class);

    @Override
    public void printLine(String text) {
        log.info(text);
    }
}
