package com.altair.exercise.infrastructure.files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class TextFileReader {

    private static final Logger log = LoggerFactory.getLogger(TextFileReader.class);

    private final FileStorage fileStorage;

    public TextFileReader(final FileStorage fileStorage) {
        this.fileStorage = fileStorage;
    }

    public List<String> readLinesFromFile(final String fileName) {
        try {
            return Files.readAllLines(
                Paths.get(fileStorage.getFile(fileName).getAbsolutePath()));
        } catch (IOException e) {
            log.error("Error reading lines from file with name: {}}", fileName, e);
            return Collections.emptyList();
        }
    }
}
