package com.altair.exercise.infrastructure.files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.InputStream;

public class FileStorage {

    private static final Logger log = LoggerFactory.getLogger(FileStorage.class);

    public InputStream getResourceAsStream(final String fileName) {
        return getClassLoader().getResourceAsStream(fileName);
    }

    public File getFile(final String fileName) {
        try {
            return new File(getClassLoader().getResource(fileName).getFile());
        }catch (Exception e) {
            log.error("There was an error getting Text.txt file from resource folder", e);
            return null;
        }
    }

    private ClassLoader getClassLoader() {
        return getClass().getClassLoader();
    }
}
