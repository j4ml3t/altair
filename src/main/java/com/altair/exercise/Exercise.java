package com.altair.exercise;

import com.altair.exercise.domain.wordcounter.Console;
import com.altair.exercise.domain.wordcounter.WordCounter;
import com.altair.exercise.infrastructure.ConsoleSlf4j;
import com.altair.exercise.infrastructure.PropertiesLoader;
import com.altair.exercise.infrastructure.files.FileStorage;
import com.altair.exercise.infrastructure.files.TextFileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Properties;

public class Exercise {

    private static final Logger log = LoggerFactory.getLogger(Exercise.class);

    public static void main(String[] args) {

        WordCounter wordCounter = getWordCounter();
        getFileNameProperty().ifPresentOrElse(
                fileName -> wordCounter.processWordsFromFile(fileName),
                () -> log.error("file.name property was not set on application.propeties file")
        );
    }

    private static WordCounter getWordCounter() {
        Console console = new ConsoleSlf4j();
        FileStorage fileStorage = new FileStorage();
        TextFileReader textFileReader = new TextFileReader(fileStorage);

       return new WordCounter(console, textFileReader);
    }

    private static Optional<String> getFileNameProperty() {
        PropertiesLoader propLoader = new PropertiesLoader();
        Properties properties = propLoader.getApplicationProperties();

        return Optional.ofNullable(properties.getProperty("file.name"));
    }
}
