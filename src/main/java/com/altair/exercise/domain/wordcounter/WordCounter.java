package com.altair.exercise.domain.wordcounter;

import com.altair.exercise.infrastructure.files.TextFileReader;

import java.util.*;

import static java.lang.String.format;

public class WordCounter {
    
	private static final String WORD_OUTPUT = "-  Word: %s  >> Occurrences: %s";

	private final Console console;
	private final TextFileReader textFileReader;

	public WordCounter(Console console, TextFileReader textFileReader) {
		this.console = console;
		this.textFileReader = textFileReader;
	}

	public void processWordsFromFile(final String fileName) {

		final List<String> lines = textFileReader.readLinesFromFile(fileName);

		final HashMap<String, Integer> wordMap = getWordMapFromLines(lines);
		
		printWordsOrderByAppearanceDesc(wordMap);
	}

	private void printWordsOrderByAppearanceDesc(final HashMap<String, Integer> wordMap) {

		final List<Map.Entry<String, Integer>> wordOrderedList = getOrderedListOfWordsDesc(wordMap);

		Collections.sort(wordOrderedList, (word1, word2) -> word2.getValue().compareTo(word1.getValue()));

		wordOrderedList.forEach(entry ->
				console.printLine(format(WORD_OUTPUT, entry.getKey(), entry.getValue())));
	}

	private HashMap<String, Integer> getWordMapFromLines(final List<String> lines) {

		final HashMap<String, Integer> wordMap = new HashMap<>();
		
		for (String line: lines) {
			for (String word: getWordsFromLine(line)) {

				if (wordMap.containsKey(word)) {
					wordMap.put(word, wordMap.get(word)+1);
				} else {
					wordMap.put(word, 1);
				}
			}
		}
		return wordMap;
	}

	private ArrayList<Map.Entry<String, Integer>> getOrderedListOfWordsDesc(final HashMap<String,Integer> wordMap) {

		return new ArrayList<>(wordMap.entrySet());
	}

	private String[] getWordsFromLine(String line) {

		return line.replaceAll("[^a-zA-Z ]", "")
					.toLowerCase()
					.split("\\s+");
	}

}
