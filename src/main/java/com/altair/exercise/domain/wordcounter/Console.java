package com.altair.exercise.domain.wordcounter;

public interface Console {

    void printLine(String text);
}
