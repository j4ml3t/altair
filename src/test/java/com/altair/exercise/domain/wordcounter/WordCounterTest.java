package com.altair.exercise.domain.wordcounter;

import com.altair.exercise.infrastructure.ConsoleSlf4j;
import com.altair.exercise.infrastructure.files.FileStorage;
import com.altair.exercise.infrastructure.files.TextFileReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.IOException;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WordCounterTest {

    public static final String TEXT_TXT = "Text.txt";
    @Mock
    private ConsoleSlf4j console;
    private WordCounter wordCounter;
    private TextFileReader textFileReader;
    @Mock
    private FileStorage fileStorage;

    @BeforeEach
    public void initialize() {
        textFileReader = new TextFileReader(fileStorage);
        wordCounter = new WordCounter(console, textFileReader);
    }

    @Test
    public void Should_PrintWordOutputOrdered_When_ReadTextFile() throws IOException {

        // Given
        File fileToTest = new File (getClass().getClassLoader().getResource(TEXT_TXT).getFile());
        when(fileStorage.getFile(TEXT_TXT)).thenReturn(fileToTest);

        // When
        wordCounter.processWordsFromFile(TEXT_TXT);

        // Then
        InOrder inOrder = inOrder(console);
        inOrder.verify(console).printLine("-  Word: lorem  >> Occurrences: 3");
        inOrder.verify(console).printLine("-  Word: ipsum  >> Occurrences: 2");
        inOrder.verify(console).printLine("-  Word: dolor  >> Occurrences: 1");
    }

}
