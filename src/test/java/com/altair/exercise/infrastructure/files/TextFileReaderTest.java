package com.altair.exercise.infrastructure.files;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TextFileReaderTest {

    @Test
    public void Should_returnLines_When_readLinesFromFile() throws IOException {
        // Given
        List<String> expectedLines = Arrays.asList("Lorem ipsum dolor,", "Lorem ipsum.", "Lorem,");
        TextFileReader textFileReader = new TextFileReader(new FileStorage());

        // When
        List<String> resultLines = textFileReader.readLinesFromFile("Text.txt");

        // Then
        assertEquals(expectedLines.size(), resultLines.size());
        assertTrue(expectedLines.containsAll(resultLines));
        assertTrue(resultLines.containsAll(expectedLines));
    }
}
